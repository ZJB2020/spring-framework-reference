[[cache-strategies]]
=== Understanding the cache abstraction
.Cache vs Buffer
****

The terms "buffer" and "cache" tend to be used interchangeably; note however they
represent different things. A buffer is used traditionally as an intermediate temporary
store for data between a fast and a slow entity. As one party would have to __wait__ for
the other affecting performance, the buffer alleviates this by allowing entire blocks of
data to move at once rather then in small chunks. The data is written and read only once
from the buffer. Furthermore, the buffers are __visible__ to at least one party which is
aware of it.

A cache on the other hand by definition is hidden and neither party is aware that
caching occurs.It as well improves performance but does that by allowing the same data
to be read multiple times in a fast fashion.

A further explanation of the differences between two can be found
http://en.wikipedia.org/wiki/Cache_(computing)#The_difference_between_buffer_and_cache[here].
****

At its core, the abstraction applies caching to Java methods, reducing thus the number
of executions based on the information available in the cache. That is, each time a
__targeted__ method is invoked, the abstraction will apply a caching behavior checking
whether the method has been already executed for the given arguments. If it has, then
the cached result is returned without having to execute the actual method; if it has
not, then method is executed, the result cached and returned to the user so that, the
next time the method is invoked, the cached result is returned. This way, expensive
methods (whether CPU or IO bound) can be executed only once for a given set of
parameters and the result reused without having to actually execute the method again.
The caching logic is applied transparently without any interference to the invoker.

[IMPORTANT]
====

Obviously this approach works only for methods that are guaranteed to return the same
output (result) for a given input (or arguments) no matter how many times it is being
executed.
====

Other cache-related operations are provided by the abstraction such as the ability
to update the content of the cache or remove one of all entries. These are useful if
the cache deals with data that can change during the course of the application.

Just like other services in the Spring Framework, the caching service is an
abstraction (not a cache implementation) and requires the use of an actual storage to
store the cache data - that is, the abstraction frees the developer from having to write
the caching logic but does not provide the actual stores. This abstraction is
materialized by the `org.springframework.cache.Cache` and
`org.springframework.cache.CacheManager` interfaces.

There are <<cache-store-configuration,a few implementations>> of that abstraction
available out of the box: JDK `java.util.concurrent.ConcurrentMap` based caches,
http://ehcache.org/[EhCache], Gemfire cache,
https://code.google.com/p/guava-libraries/wiki/CachesExplained[Guava caches] and
JSR-107 compliant caches. See <<cache-plug>> for more information on plugging in
other cache stores/providers.

[IMPORTANT]
====
The caching abstraction has no special handling of multi-threaded and multi-process
environments as such features are handled by the cache implementation. .
====

If you have a multi-process environment (i.e. an application deployed on several nodes),
you will need to configure your cache provider accordingly. Depending on your use cases,
a copy of the same data on several nodes may be enough but if you change the data during
the course of the application, you may need to enable other propagation mechanisms.

Caching a particular item is a direct equivalent of the typical get-if-not-found-then-
proceed-and-put-eventually code blocks found with programmatic cache interaction: no locks
are applied and several threads may try to load the same item concurrently. The same applies
to eviction: if several threads are trying to update or evict data concurrently, you may
use stale data. Certain cache providers offer advanced features in that area, refer to
the documentation of the cache provider that you are using for more details.

To use the cache abstraction, the developer needs to take care of two aspects:

* caching declaration - identify the methods that need to be cached and their policy
* cache configuration - the backing cache where the data is stored and read from


