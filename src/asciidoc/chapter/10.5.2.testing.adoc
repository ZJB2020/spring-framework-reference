[[spring-mvc-test-server-performing-requests]]
====== Performing Requests
To perform requests, use the appropriate HTTP method and additional builder-style
methods corresponding to properties of `MockHttpServletRequest`. For example:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(post("/hotels/{id}", 42).accept(MediaType.APPLICATION_JSON));
----

In addition to all the HTTP methods, you can also perform file upload requests, which
internally creates an instance of `MockMultipartHttpServletRequest`:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(fileUpload("/doc").file("a1", "ABC".getBytes("UTF-8")));
----

Query string parameters can be specified in the URI template:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(get("/hotels?foo={foo}", "bar"));
----

Or by adding Servlet request parameters:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(get("/hotels").param("foo", "bar"));
----

If application code relies on Servlet request parameters, and doesn't check the query
string, as is most often the case, then it doesn't matter how parameters are added. Keep
in mind though that parameters provided in the URI template will be decoded while
parameters provided through the `param(...)` method are expected to be decoded.

In most cases it's preferable to leave out the context path and the Servlet path from
the request URI. If you must test with the full request URI, be sure to set the
`contextPath` and `servletPath` accordingly so that request mappings will work:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(get("/app/main/hotels/{id}").contextPath("/app").servletPath("/main"))
----

Looking at the above example, it would be cumbersome to set the contextPath and
servletPath with every performed request. That's why you can define default request
properties when building the `MockMvc`:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	public class MyWebTests {

		private MockMvc mockMvc;

		@Before
		public void setup() {
			mockMvc = standaloneSetup(new AccountController())
				.defaultRequest(get("/")
				.contextPath("/app").servletPath("/main")
				.accept(MediaType.APPLICATION_JSON).build();
		}
----

The above properties will apply to every request performed through the `MockMvc`. If the
same property is also specified on a given request, it will override the default value.
That is why, the HTTP method and URI don't matter, when setting default request
properties, since they must be specified on every request.

